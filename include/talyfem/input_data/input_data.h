/*
  Copyright 2014-2017 Baskar Ganapathysubramanian

  This file is part of TALYFem.

  TALYFem is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.

  TALYFem is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with TALYFem.  If not, see <http://www.gnu.org/licenses/>.
*/
// --- end license text --- //
#ifndef INPUTDATA_INPUTDATA_H_
#define INPUTDATA_INPUTDATA_H_

#include <libconfig.h++>

#include <fstream>
#include <iostream>
#include <map>
#include <string>  // for std::string
#include <vector>  // for std::vector

#include <talyfem/common/exceptions.h>
#include <talyfem/utils/utils.h>
#include <talyfem/basis/basis.h>  // for kBasisFunction


namespace TALYFEMLIB {

/**
 * This structure stores basic parameters to define FEM problem
 * Parameters are read from a config file via libconfig
 *
 * This struct can be extended (see HeatTransient example)
 */
class InputData {
 public:
  int nsd;  ///< number of space dimensions: 1,2,3
  bool ifBoxGrid;  ///< if Grid of box type
  bool ifTriElem;  ///< if Grid of triangle type
  kBasisFunction basisFunction;  ///< basis function to use
  int basisRelativeOrder;  ///< relative order of basis function
  bool ifDD;  ///< whether to use Domain Decomposition (DD)
  double* L;  ///< array of lengths Lx,Ly,Lz
  int* Nelem;  ///< array of number of elements in directions x,y,z.
  bool ifPrintStat;  ///< if printing of status items is desired
  bool ifPrintLog;  ///< if printing of log items is desired
  bool ifPrintWarn;  ///< if printing of warning items is desired
  bool ifPrintInfo;  ///< if printing of info items is desired
  bool ifPrintTime;  ///< if printing of time items is desired
  std::string inputFilenameGridField;  ///< filename with grid
  std::string inputFilenameGrid;  ///< filename with gridField to be loaded from
  std::string varListInput;  ///< load data from file accoring to list of
                             ///< nodal variables
  std::string varListOutput;  ///< output data to file accoring to list of
                              ///< nodal variables
  int typeOfIC;  ///< 0-load from file
  bool ifLoadNodeIndicators;  ///< try to load node indicators from
                              ///< inputFilenameGrid. This only works when the
                              ///< grid is loaded and not generated
  bool ifWriteNodeIndicators;  ///< write node indicators in save_gf()

  libconfig::Config cfg;  ///< libconfig config object

  InputData();

  virtual ~InputData();

  /**
   * read parameters from config file
   */
  virtual bool ReadFromFile(
      const std::string& filename = std::string("config.txt"));

  /**
   * check the coherency of input data
   */
  virtual bool CheckInputData() const;

  /**
   * Read values from the config file.
   *
   * This must be called prior to trying to read specific config values.
   *
   * @param filename Name of config file
   */
  void ReadConfigFile(std::string filename);

  /**
   * Initialize the inputdata (parses data from file to member fields)
   */
  virtual bool Initialize();

  /**
   * Reads the value of the given key from the configuration file and
   * stores it in the given value variable. The variable and its value is
   * output if the appropriate flags are set.
   *
   * @param key_name The name of the parameter to fetch
   * @param value Variable that will be filled with the value fetched
   * @return true if read is successful
   */
  template<typename T>
  bool ReadValue(const std::string& key_name, T& value) {
    bool result = cfg.lookupValue(key_name, value);
    if (result) {
      // the ifPrintStat test is required here to prevent printing the value
      // of ifPrintStat when it differs from the default. PrintStatus
      // relies on a global value that is set from ifPrintStat. However, this
      // value is not set until after this function exits. So if we fail to
      // check for this here, we may have incorrect behaviour.
      if (ifPrintStat)
        PrintStatusStream(std::cerr, "", key_name, ": ", value);
    }
    return result;
  }

  /**
   * Reads an array from the input file and stores it in the given variable.
   *
   * The array length is validated against an expected value, thorwing an
   * exception if they don't match.
   *
   * The user is required to pass a pointer to store the the array date. The
   * pointer must be unitialized and the memory will be allocated within the
   * function. The user is responsible for freeing the memory. Memory will only
   * be allocated if the input file has an array of proper length associated
   * with the given key.
   *
   * @param key_name the key name of the configuation value to retrieve
   * @param[out] arr array to store the data (will be allocated here)
   * @param expected_count expected length of the array to read
   * @throw TALYException if key points to a value that is not an array
   * @throw TALYException if array length is incorrect
   * @return true if the array was found in the input file
   */
  template <typename T>
  bool ReadArray(const std::string& key_name, T* &arr, int expected_count) {
    if (!ValidateIsArray(key_name, expected_count)) { return false; }
    libconfig::Setting &setting = cfg.lookup(key_name);

    arr = new T[expected_count];
    for (int i = 0; i < expected_count; i++) {
      T value = setting[i];
      arr[i] = value;
      PrintStatusStream(std::cerr, "", key_name, "[", i, "]: ", value);
    }
    return true;
  }

  /**
   * Reads an array from the input file and stores it in the given vector.
   *
   * The array length is validated against an expected value, throwing an
   * exception if they don't match.
   *
   * The given vector is resized to fit the data. Any existing data in the
   * vector is lost. Resizing will only happen if the input file has an array
   * of proper length associated with the given key.
   *
   * @param key_name the key name of the configuation value to retrieve
   * @param[out] arr vector to store the data (will be allocated here)
   * @param expected_count expected length of the array to read
   * @throw TALYException if key points to a value that is not an array
   * @throw TALYException if array length is incorrect
   * @return true if the array was found in the input file
   */
  template <typename T>
  bool ReadArray(const std::string& key_name, std::vector<T>& arr,
                 int expected_count) {
    if (!ValidateIsArray(key_name, expected_count)) { return false; }
    libconfig::Setting &setting = cfg.lookup(key_name);

    arr.resize(expected_count);
    for (int i = 0; i < expected_count; i++) {
      T value = setting[i];
      arr[i] = value;
      PrintStatusStream(std::cerr, "", key_name, "[", i, "]: ", value);
    }
    return true;
  }

  /**
   * output all field variables to output stream
   */
  virtual std::ostream& print(std::ostream& oss) const;

  /**
   * Output list of possible variable along with default values to output stream
   */
  virtual std::ostream& printAll(std::ostream& oss) const;

  /**
   * define operator sending to stream
   */
  friend std::ostream& operator <<(std::ostream& oss, const InputData& inData) {
    return inData.print(oss);
  }

 private:
  /**
   * Confirms that the given key points to an array.
   *
   * An exception is thrown if this is not an array.
   *
   * @param key_name the key name of the configuation value to retrieve
   * @throw TALYException if key points to a value that is not an array
   * @return true if the key points to an array in the input file
   */
  bool ValidateIsArray(const std::string& key_name) {
    // this is here because lookup throws an exception if not found
    if (!cfg.exists(key_name)) {
      return false;
    }

    libconfig::Setting &setting = cfg.lookup(key_name);
    if (!setting.isArray()) {  // confirm this is an array
      throw TALYException() << "expected array input but found single value"
                            << " (key = " << key_name << ")";
    }
    return true;
  }

  /**
   * Confirms that the given key points to an array of given length.
   *
   * The array length is validated against an expected value, throwing an
   * exception if they don't match.
   *
   * @param key_name the key name of the configuation value to retrieve
   * @param expected_count expected length of the array to read
   * @throw TALYException if key points to a value that is not an array
   * @throw TALYException if array length is incorrect
   * @return true if a proper length array was found in the input file
   */
  bool ValidateIsArray(const std::string& key_name, int expected_count) {
    // first make sure this is an array
    if (!ValidateIsArray(key_name)) { return false; }

    libconfig::Setting &setting = cfg.lookup(key_name);
    if (setting.getLength() != expected_count) {  // validate length
      throw TALYException() << "input array is wrong length"
                            << " (key = " << key_name << ")\n"
                            << "    expected length = " << expected_count
                            << " actual length = " << setting.getLength();
    }
    return true;
  }
};

}  // namespace TALYFEMLIB

#endif  // INPUTDATA_INPUTDATA_H_
