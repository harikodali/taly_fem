/*
  Copyright 2014-2016 Baskar Ganapathysubramanian

  This file is part of TALYFem.

  TALYFem is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.

  TALYFem is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with TALYFem.  If not, see <http://www.gnu.org/licenses/>.
*/
// --- end license text --- //
#include <talyfem/grid/zeroptv.h>

#include <math.h>
#include <stdio.h>
#include <algorithm>  // for std::min

namespace TALYFEMLIB {

void ZEROPTV::crossProduct(const ZEROPTV& u, const ZEROPTV& v) {
  // (u x v)_i = e_ijk u_j v_k;
  (*this)(0) = -u(2) * v(1) + u(1) * v(2);
  (*this)(1) = -u(0) * v(2) + u(2) * v(0);
  (*this)(2) = -u(1) * v(0) + u(0) * v(1);
}

double ZEROPTV::innerProduct(const ZEROPTV& B) const {
  return position_[0] * B.position_[0] +
         position_[1] * B.position_[1] +
         position_[2] * B.position_[2];
}

double ZEROPTV::Normalize() {
  const double len = norm();
  position_[0] /= len;
  position_[1] /= len;
  position_[2] /= len;
  return len;
}

double ZEROPTV::SafeNormalize() {
  const double len = norm();
  if (len < 1e-14) {
    position_[0] = 0.0;
    position_[1] = 0.0;
    position_[2] = 0.0;
    return 0.0;
  }

  position_[0] /= len;
  position_[1] /= len;
  position_[2] /= len;
  return len;
}

double ZEROPTV::norm() const {
  return sqrt(position_[0] * position_[0] + position_[1] * position_[1] +
              position_[2] * position_[2]);
}

double ZEROPTV::distanceTo(const ZEROPTV& Q) const {
  const ZEROPTV& P = *this;
  return sqrt((P.x() - Q.x()) * (P.x() - Q.x()) +
              (P.y() - Q.y()) * (P.y() - Q.y()) +
              (P.z() - Q.z()) * (P.z() - Q.z()));
}

double ZEROPTV::angleTo(const ZEROPTV& Q) const {
  const ZEROPTV& P = *this;
  ZEROPTV R;
  R.x() = P.x() * Q.x() + P.y() * Q.y();
  R.y() = P.x() * Q.y() - Q.x() * P.y();
  R.Normalize();

  const double theta = acos(R.x());
  if (R.y() > 0)
    return theta;
  return -theta;
}

double ZEROPTV::distanceTo(const ZEROPTV& P, const ZEROPTV& Q) const {
  const ZEROPTV& A = *this;
  const double PA = A.distanceTo(P);
  ZEROPTV pPA;
  pPA.x() = A.x() - P.x();
  pPA.y() = A.y() - P.y();
  ZEROPTV pPQ;
  pPQ.x() = Q.x() - P.x();
  pPQ.y() = Q.y() - P.y();
  pPQ.Normalize();
  const double PD = pPA.innerProduct(pPQ);
  const double d = sqrt(fabs(PA * PA - PD * PD));
  if (pPQ.angleTo(pPA) > 0)
    return -d;
  return d;
}

double ZEROPTV::minimumDistanceTo(const ZEROPTV& P, const ZEROPTV& Q) const {
  const ZEROPTV& A = *this;
  const double lPA = A.distanceTo(P);
  ZEROPTV pPA;
  pPA.x() = A.x() - P.x();
  pPA.y() = A.y() - P.y();
  ZEROPTV pPQ;
  pPQ.x() = Q.x() - P.x();
  pPQ.y() = Q.y() - P.y();
  const double lPQ = pPQ.Normalize();
  const double lPD = pPA.innerProduct(pPQ);
  const double d = sqrt(fabs(lPA * lPA - lPD * lPD));
  if (lPD >= 0 && lPD <= lPQ) {
    return fabs(d);
  }
  return std::min(A.distanceTo(P), A.distanceTo(Q));
}

int ZEROPTV::directionOf(const ZEROPTV& P, const ZEROPTV& Q) const {
  ZEROPTV e;
  ZEROPTV normal;
  ZEROPTV distance;
  e.x() = Q.x() - P.x();
  e.y() = Q.y() - P.y();
  e.z() = 0;

  normal.x() = e.y();
  normal.y() = -e.x();
  normal.z() = 0;

  distance.x() = (*this).x() - P.x();
  distance.y() = (*this).y() - P.y();
  distance.z() = 0;

  const double inner = normal.innerProduct(distance);
  if (fabs(inner) < 1e-15)
    return 0;
  if (inner > 0)
    return 1;
  return -1;
}

void ZEROPTV::print() const {
  printf("%e %e %e", x(), y(), z());
}

}  // namespace TALYFEMLIB
